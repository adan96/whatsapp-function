﻿using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace whatsapp_covid19{
    class API{

        public static async Task<string> getMessage(string state){

            //Stadistics API
            string apigeeKey = "FRilvIvAivRE8A03lSoCNA2Fo2uGL2GE";
            string apigeeBasepath = "https://theapigeeteam-eval-test.apigee.net/covid/";
            string url = apigeeBasepath + "casos?apikey=" + apigeeKey;

            //Set the API to json variable and converting it to a string
            WebClient wc = new WebClient();
            var json = wc.DownloadString(url);
            int valor = 0;

            //Deserializing json
            Body[] bodyList = JsonConvert.DeserializeObject<Body[]>(json);

            //Check into API if exist state
            for (int i = 0; i < 32; i++){

                if (bodyList[i].estado == state){

                    valor = 1;
                    break;

                }

            }

            if (valor == 1){

                //If exist, cast the state into url
                url = apigeeBasepath + "casos/" + state + "?apikey=" + apigeeKey;

                //Set the new API to json variable and converting it to a string
                json = wc.DownloadString(url);

                //Deserializing json
                Body body = JsonConvert.DeserializeObject<Body>(json);

                //Casting json variable, this is the information that will be sent
                json = "\nEstado: " + body.estado
                        + "\nConfirmados: " + body.confirmados
                        + "\nSospechosos: " + body.sospechosos
                        + "\nNegativos: " + body.negativos
                        + "\nMuertes: " + body.muertes
                        + "\nMujeres confirmadas: " + body.sexoConfirmados.mujeres
                        + "\nHombres confirmados: " + body.sexoConfirmados.hombres;

            }else{

                //If doesn´t exist the state, send state keys
                json = "Escriba uno de los estados (clave de tres letras): ";
                for (int i = 0; i < 32; i++)
                    json += "\n" + (i + 1) + ": " + bodyList[i].estado;

            }

            //return json variable independently the information of the conditionals
            return json;

        }

    }
}