﻿using System;

namespace whatsapp_covid19{
    class Body{

        public String estado { get; set; }
        public int confirmados { get; set; }
        public int sospechosos { get; set; }
        public int negativos { get; set; }
        public int muertes { get; set; }
        public SexoConfirmados sexoConfirmados { get; set; }

    }

    class SexoConfirmados{

        public int hombres { get; set; }
        public int mujeres { get; set; }

    }

}