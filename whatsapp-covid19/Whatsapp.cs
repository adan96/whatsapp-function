using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Twilio.TwiML;
using System.Linq;

namespace whatsapp_covid19{
    public static class Whatsapp{
        [FunctionName("Whatsapp")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log){

            //Receiving parameter
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var formValues = requestBody.Split('&').Select(value => value.Split('='))
                .ToDictionary(pair => Uri.UnescapeDataString(pair[0]).Replace("+", " "),
                              pair => Uri.UnescapeDataString(pair[1]).Replace("+", " "));

            var state = formValues["Body"];

            state = state.ToString().ToUpper();

            //Receiving API String with state
            var json = await API.getMessage(state);

            //Http Request variable for Twilio
            var response = new MessagingResponse().Message(json);
            var twiml = response.ToString();

            //Converting to String
            return new ContentResult{

                Content = twiml,
                ContentType = "application/xml"

            };

        }
    }
}
